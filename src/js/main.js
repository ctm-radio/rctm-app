/*
 * main.js -- Main application logic
 *
 * Copyright 2013 - 2020 Felipe Peñailillo <breadmaker@ctmradio.org>
 *                       William Cabrera   <cabrerawilliam@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var firstTimeAPI = true,
  firstTimeSocket = true,
  hideLicenseTimer = undefined;

console.time("Ready event fired");
console.time("Connected to socket");
console.time("Connected to API");

function displayLicenseInfo(license) {
  let licenseDisplay = document.getElementById("license-display");
  licenseDisplay.innerHTML = "<i class=\"icon-cc\"></i>";
  console.log(license.split(" ")[1].split("-"));
  license.split(" ")[1].split("-").forEach((item) => {
    // licenseDisplay.appendChild(item.toLowerCase());
    let icon = document.createElement("i");
    icon.classList.add("icon-cc-" + item.toLowerCase());
    licenseDisplay.appendChild(icon);
  });
  if (licenseDisplay.classList.contains("hidden")) {
    licenseDisplay.classList.remove("hidden");
    if (hideLicenseTimer !== undefined) {
      clearTimeout(hideLicenseTimer);
    }
    hideLicenseTimer = setTimeout(function() {
      licenseDisplay.classList.add("hidden");
    }, 5000);
  }
}

function fetchAPI(info) {
  let url = "https://api.ctmradio.org/?img_size=50";
  if (info !== undefined) {
    let artist = info.artist;
    let title = info.title;
    url = "https://api.ctmradio.org/?artist=" + encodeURIComponent(artist) + "&title=" + encodeURIComponent(title) + "&img_size=50";
  }
  fetch(url).then(r => r.json()).then(function(data) {
    if (firstTimeAPI) {
      console.timeEnd("Connected to API");
      firstTimeAPI = false;
      let elem = document.getElementById("startup");
      elem.parentNode.removeChild(elem);
    }
    document.getElementById("cover").style = "background-image: url(" + data.cover + ")";
    document.getElementById("cover").src = "https://api.ctmradio.org/cover/" + data.album_id + ".png";
    document.getElementById("title").textContent = data.title;
    document.getElementById("artist").textContent = data.artist;
    document.getElementById("country").textContent = "(" + data.country + ")";
    document.getElementById("album").textContent = data.album;
    document.getElementById("year").textContent = "(" + data.year + ")";
    document.getElementById("license").textContent = data.license.shortname;
    displayLicenseInfo(data.license.shortname);
  }).catch(function(e) {
    console.error(e);
    console.error("Can't connect to API");
  });
}

function initSocket() {
  let ctmws, currentMetadata = {
    artist: "",
    title: ""
  };
  const socketMessageListener = (event) => {
    if (event.data !== "ping") {
      var data = JSON.parse(event.data);
      if (currentMetadata.title !== data.title || currentMetadata.artist !== data.artist) {
        currentMetadata = data;
        fetchAPI(data);
      }
    }
  };
  const socketOpenListener = () => {
    if (firstTimeSocket) {
      console.timeEnd("Connected to socket");
      firstTimeSocket = false;
    } else {
      console.info("Reconnected to socket.");
    }
    // TODO: Update UI accordingly
  };
  const socketCloseListener = () => {
    if (ctmws) {
      console.error("Socket disconnected");
      // TODO: Notify error on UI
    }
    ctmws = new WebSocket("wss://ws.ctmradio.org");
    ctmws.addEventListener("open", socketOpenListener);
    ctmws.addEventListener("message", socketMessageListener);
    ctmws.addEventListener("close", socketCloseListener);
  };
  socketCloseListener();
}

function ready(fn) {
  if (document.readyState != "loading") {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

ready(function() {
  console.timeEnd("Ready event fired");
  initSocket();
  document.getElementById("cover-wrapper").addEventListener("click", function() {
    document.getElementById("license-display").classList.toggle("hidden");
    if (hideLicenseTimer !== undefined) {
      clearTimeout(hideLicenseTimer);
    }
  });
});
