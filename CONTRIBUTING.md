# Contribution guide #

If you want to contribute your modifications to the project, it is quite simple,
the first thing you should do is clone this repository:

```bash
$ git clone git@gitlab.com:ctm-radio/rctm-app.git
```

After making your modifications in the source files, located at `src`, you must
build the result, for that you need to install what you need using `npm`:

```bash
$ cd path/to/rctm-app
$ npm install
```

Then, you can build with this command:

```bash
$ npm run build
```

Optionally, you can enable watching of files, which will automatically build
every time you save any source files, with the following command:

```bash
$ npm run build:watch
```

Finally, you can start a temporary test server using Python. For Python 3:

```bash
$ python -m http.server 5000
```

Or if you have Python 2:

```bash
$ python -m SimpleHTTPServer 5000
```

The number argument sets the port for the server (defaults to 8000)

If there's a building error, it will tell you immediately so that you can
correct it. Once the changes are made, you can
[create a Merge Request](https://gitlab.com/ctm-radio/rctm-app/-/merge_requests/new)
to be reviewed.

If you can't find a solution for your build error, you can then
[report the Issue](https://gitlab.com/ctm-radio/rctm-app/-/issues/new),
including as much information as possible.
