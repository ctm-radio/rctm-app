# Web Application for CTM Radio #

This is the CTM Radio application repository designed to work natively under a
browser as a Web application.

## Building from source ##

If you wish to help the project with code, please check the `CONTRIBUTING.md`
file.

## FAQ ##

### Configuration is not saved ###

Probably your browser version is old or it does not meet HTML5 standards, and/or
does not support [LocalStorage](http://caniuse.com/#feat=namevalue-storage).

In some recently old versions of Firefox, you can try to enable LocalStorage, by
going to the advanced browser configuration, at `about:config` and searching for
the `dom.storage.enabled` setting that should be enabled.
